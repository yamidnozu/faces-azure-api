class IdenticalRsMapper:
    def __init__(self, isIdentical, value ):
        self.isIdentical=isIdentical
        self.value=value

    def serialize(self):
        return {
            "isIdentical":self.isIdentical,
            "value":self.value
        }
class DetectFacesRsMapper:
    def __init__(self, count, hasFaces ):
        self.count=count
        self.hasFaces=hasFaces

    def serialize(self):
        return {
            "count":self.count,
            "hasFaces":self.hasFaces
        }