# Guía

## Poner ambiente (venv) en windows
Comando

````
venv\Scripts\activate.bat
````


## Versión base de
>  ***python 3.9.5***

Instalar dependencias

````
pip install -r requirements.txt
````
Correr server:

````
python -i main.py
````

### Petición:

Cuerpo body:

> La key y endpoint se deben sacar de la cuenta de azure
{

    key: <clave1 o clave2 de azure para suscripción a Faces (obtener de la cuenta de azure)>

    endpoint: <Endpoint del recurso brindado por azure (obtener de la cuenta de azure)>

    person1: {
     url: <la url de la foto de la foto 1 a comparar>
    }

    person2: {
      url: <la url de la foto de la foto 2 a comparar>
    }

}


#### Ejemplo detectar si los rostros son iguales:

Petición tipo POST
Ruta: http://127.0.0.1:5000/v1/facial
Cuerpo de ejemplo:
````
{
    "key": "4ba4bb8a39cc4985b79ea5e8b66dbb6c",
    "endpoint": "https://centro-suryamid.cognitiveservices.azure.com/",
    "person1": {
        "url": "https://www.eje21.com.co/site/wp-content/uploads/2018/09/Don-Ram%C3%B3n.-Foto-Televisa..png"
    },
    "person2": {
        "url": "https://www.oronoticias.com.mx/wp-content/uploads/2019/05/Don-Ramon-P.jpg"
    }
}


````
  

#### Saliida
````
{
"isIdentical": true,
"value": 0.83391
}
````



# Ejemplo detección de caras

Petición tipo POST
Ruta: http://127.0.0.1:5000/v1/detect-faces
Cuerpo de ejemplo:
````
{
    "key": "4ba4bb8a39cc4985b79ea5e8b66dbb6c",
    "endpoint": "https://centro-suryamid.cognitiveservices.azure.com/",
    "url": "https://img.blogs.es/anexom/wp-content/uploads/2019/02/caras-inteligencia-780x515.jpg"
}


````
  

#### Saliida
````
{
    "count": 15,
    "hasFaces": true
}
````

