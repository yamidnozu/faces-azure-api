# Imports para flask
from models.models import DetectFacesRsMapper, IdenticalRsMapper
from flask import Flask, request
import json
# Azure face
from azure.cognitiveservices.vision.face import FaceClient
from msrest.authentication import CognitiveServicesCredentials




app = Flask(__name__) 




@app.route("/v1/facial", methods=["POST"])
def facial():
    body = json.loads(request.data)
    person1 = body["person1"]
    person2 = body["person2"]
    key = body["key"]
    endpoint = body["endpoint"]
    urlImage1=person1["url"]
    urlImage2=person2["url"]
    
    # Inicio para reconocimiento de rostros
    face_client = FaceClient(endpoint, CognitiveServicesCredentials(key))
    target_image_file_names = [urlImage1,urlImage2]
    detected_faces2 = face_client.face.detect_with_url(urlImage2, detection_model='detection_03')
    source_image2_id = detected_faces2[0].face_id

    detected_faces_ids = []
    for image_file_name in target_image_file_names:
        detected_faces = face_client.face.detect_with_url(image_file_name, detection_model='detection_03')
        detected_faces_ids.append(detected_faces[0].face_id)

    verify_result_diff = face_client.face.verify_face_to_face(source_image2_id, detected_faces_ids[0])
    response = IdenticalRsMapper(verify_result_diff.is_identical,verify_result_diff.confidence)
    # Respuesta del servidor
    return response.serialize(), 200





@app.route("/v1/detect-faces", methods=["POST"])
def detectFace():
    body = json.loads(request.data)
    url = body["url"]
    key = body["key"]
    endpoint = body["endpoint"]
    
    # Inicio para reconocimiento de rostros
    face_client = FaceClient(endpoint, CognitiveServicesCredentials(key))
    single_face_image_url = url
    detected_faces = face_client.face.detect_with_url(url=single_face_image_url, detection_model='detection_03')

    # Respuesta del servidor
    if not detected_faces:
        response = DetectFacesRsMapper(0, False)
        return response.serialize(), 200
    else:
        response = DetectFacesRsMapper(len(detected_faces), len(detected_faces) > 0 )
        return response.serialize(), 200





# Inicio de servidor
if(__name__ == "__main__"):
    app.run()


